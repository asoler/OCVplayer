import numpy as np
import cv2

thresh = 10

def on_thresh(val):
    global thresh
    thresh = val
    print('real thresh value:', (float((-20+2.5*thresh)/100)))

cv2.createTrackbar("tresh", "main", thresh, 17, on_thresh)

def process(img, **kwargs):
    ir_channel = img[:, :, 3]
    bgr_img = img[:,:,:3]
    red_channel = cv2.blur(src=img[:, :, 2], ksize=(3,3))
    
    # NDVI calculation
    n = (ir_channel.astype(np.float32)-red_channel.astype(np.float32)) 
    d = (ir_channel.astype(np.float32)+red_channel.astype(np.float32))
    
    n[d==0] = 0
    d[d==0] = 1
    ndvi = np.divide(n.astype(np.float32),d.astype(np.float32))
    ##NDVI calculation
    # coef = 2
    # n = (coef*ir_channel.astype(np.float32)-red_channel.astype(np.float32))
    # d = (coef*ir_channel.astype(np.float32)+red_channel.astype(np.float32))
    # ndvi = np.divide(n.astype(np.float32),d.astype(np.float32))

    ndvi_masked = np.zeros(ndvi.shape, dtype=np.uint8)
    ndvi_masked[np.where(ndvi > (float((-20+2.5*thresh)/100)))] = 255 
    ndvi_masked = cv2.bitwise_and(bgr_img, bgr_img, mask=ndvi_masked)

    return bgr_img, ndvi_masked

def post_process(img, res, **kwargs):
    rgb_img, ndvi_masked = res
    concat = np.concatenate((rgb_img, ndvi_masked), axis=1)
    return concat