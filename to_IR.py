
import numpy as np
import cv2

thresh = 127

def on_thresh(val):
    global thresh
    thresh = val

cv2.createTrackbar("tresh", "main", thresh, 255, on_thresh)

def process(img, **kwargs):
    ir_channel = img[:, :, 3]
    bgr_img = img[:,:,:3]
    IR_masked = np.zeros(ir_channel.shape, dtype=np.uint8)
    IR_masked[np.where(ir_channel > thresh)] = 255 
    IR_masked = cv2.bitwise_and(bgr_img, bgr_img, mask=IR_masked)
    return bgr_img, IR_masked


def post_process(img, res, **kwargs):
    rgb_img, IR_masked = res
    concat = np.concatenate((rgb_img, IR_masked), axis=1)
    return concat