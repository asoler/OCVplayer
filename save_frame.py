import cv2
import numpy as np

i = 0
gray = None


def process(img, **kwargs):
    global gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.bitwise_not(gray)
    return gray


def post_process(img, res, **kwargs):
    return res


def key_cb(key_code):
    global i, gray
    if key_code == ord('s'):
        fname = '{:06}.png'.format(i)
        print("writing " + fname)
        cv2.imwrite(fname, gray)
        i += 1
