/*
 * OCVplayer.cpp
 *
 *  Largely based on the image_aligment.cpp OpenCV sample program
 *
 *  Created on: Mars 22, 2019
 *      Author: Stan Borkowski
 */

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <ctype.h>
#include <iostream>
#include <stdio.h>
//#include <experimental/filesystem>


using namespace cv;
using namespace std;

const std::string about =
        "Play a video or an image sequence."
        "Videos are first decoded and written into /tmp/ and then read as a sequence."
        "Note that image sequence numbering must start at 1, otherwise OpenCV VideoCapture can'r parse the sequence.\n"
        "  Usage example (camera/video/image sequence):\n"
        "\t./OCVplayer -s=2 1/video.mpg/img%%04d.png\n";

const std::string keys =
        "{@device_or_video_or_sequence | <none> | video device number or video or image sequence }"
        "{s scale        | 1.0 | image scale for main window }"
        "{b backward     | false | enable backward browsing }"
        "{h help | | print help message }"
;

const int SPEED[] = {0, 1000, 500, 200, 30, 1};

static void process(Mat& I) {
    // create a new branch in git to test specific processing
}


int main(int argc, char** argv) {
#if defined(_WIN32)
    cout << "WARNING: this program was not tested on Windows and might not work properly." << endl;
#endif

    CommandLineParser parser(argc, argv, keys);
    parser.about(about);

    if (parser.has("help")) {
        parser.printMessage();
        return 0;
    }

    string input = parser.get<string>("@device_or_video_or_sequence");
    //float scale = parser.get<float>("s");
    if (!parser.check()) {
        parser.printErrors();
        return 1;
    }

    VideoCapture capture;
    Mat frame;

//    // if input is a video decode it into a sequence
// rather llok for '%' in filename
//    if(input.find(".mp4")) {
//        // check if /tmp/input directory exist and create it
//        filesystem::exists();
//        capture.open(input);
//        for (int i = 0;;) {
//            capture >> frame;
//            if (frame.empty())
//                break;
//            sprintf(filename,"%.3d.png",i++);
//            imwrite(filename,frame);
//        }
//        capture.release();
//    }

    capture.set(CAP_PROP_FPS, 2.0);
    if (input.length() == 1) {
        capture.open(stoi(input));
    } else {
        capture.open(input);
    }
    if (!capture.isOpened()) {
        cerr << "Failed to open the video file or image sequence!\n" << endl;
        parser.printErrors();
        parser.printMessage();
        return 1;
    }
    cout<< "Frames: " << capture.get(CAP_PROP_FRAME_COUNT) <<endl;
    cout<< "Mode: " << capture.get(CAP_PROP_MODE) <<endl;
    cout<< "fps: " << capture.get(CAP_PROP_FPS) <<endl;

    string window_name = "video | q or esc to quit";
    namedWindow(window_name, WINDOW_KEEPRATIO);

    int speed_index = 0;
    int old_speed_index = 4;
    enum directions {
        FORWARD = 1, BACKWARD = -1
    } direction;  // playback direction
    direction = FORWARD;
    for (int i = 0;;) {
        capture >> frame;
        if (frame.empty())
            break;

        int64_t t = getTickCount();
        process(frame);
        double elapsed = ((double) getTickCount() - t) / getTickFrequency();
        i++;

        // draw elapsed time in millis on frame
        char str[64];
        sprintf(str, "frame: %d process: %f ms", i, 1000.0 * elapsed);
        putText(frame, str, Point2d(10, 20), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 0, 255), 1, LINE_AA);

        imshow(window_name, frame);

        char key = (char) waitKey(SPEED[speed_index]);
        switch (key) {
            case 'q':
            case 'Q':
            case 27: //escape key
                return 0;
            case 'e':
                if (direction == FORWARD && speed_index == 0)
                    direction = BACKWARD;
                if (direction == FORWARD && speed_index > 0)
                    speed_index--;
                if (direction == BACKWARD && speed_index < 5)
                    speed_index++;
                break;
            case 'r':
                if (direction == BACKWARD && speed_index == 0)
                    direction = FORWARD;
                if (direction == BACKWARD && speed_index > 0)
                    speed_index--;
                if (direction == FORWARD && speed_index < 5)
                    speed_index++;
                break;
            case ' ':
                if (speed_index == 0) {
                    speed_index = old_speed_index;
                } else {
                    old_speed_index = speed_index;
                    speed_index = 0;
                }
                break;
//            case 's': //Save an image
//                sprintf(filename,"filename%.3d.jpg",n++);
//                imwrite(filename,frame);
//                cout << "Saved " << filename << endl;
//                break;
            default:
                break;
        }
    }
    return 0;
}
