import cv2
import numpy as np

min_ab = 13
max_ab = 216


def on_ab_min(val):
    global min_ab
    min_ab = val


def on_ab_max(val):
    global max_ab
    max_ab = val


cv2.createTrackbar("min a-b", "main", min_ab, 255, on_ab_min)
cv2.createTrackbar("max a-b", "main", max_ab, 255, on_ab_max)


def process(img, **kwargs):
    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    mask = cv2.inRange(a-b, min_ab, max_ab)
    ab = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    return ab


def post_process(img, res, **kwargs):
    concat = np.concatenate((img, res), axis=1)
    return concat
