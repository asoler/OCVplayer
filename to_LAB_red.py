#################################################################################
## Warning:                                                                    ##
## This file is used in the Wiki documentation.                                ##
## Any change done here must be postponed in the associated documentation.     ##
#################################################################################

import cv2
import numpy as np

MIN_AB = 128
UPPER = 130


def on_ab_min(val):
    global MIN_AB
    MIN_AB = val


def on_a_max(val):
    global UPPER
    UPPER = val


cv2.createTrackbar("min a-b", "main", MIN_AB, 255, on_ab_min)
cv2.createTrackbar("max a*", "main", UPPER, 255, on_a_max)


def process(img, **kwargs):

    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
    l, a, b = cv2.split(lab)
    mask_1 = cv2.inRange(a-b, MIN_AB, 128)

    mask_2 = cv2.inRange(a, 0, UPPER)

    mask_3 = cv2.bitwise_or(mask_1, mask_2)
    ftot = cv2.bitwise_and(img, img, mask=mask_3)
    
    return ftot


def post_process(img, res, **kwargs):

    concat = np.concatenate((img, res), axis=1)
    
    return concat
