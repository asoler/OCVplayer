import cv2
import numpy as np


class Calibration:
    def __init__(self):
        self.objp = np.zeros((10 * 6, 3), np.float32)
        self.objp[:, :2] = np.mgrid[0:10, 0:6].T.reshape(-1, 2)
        self.objpoints = []  # 3d point in real world space
        self.imgpoints = []  # 2d points in image plane
        self.grey = None

    def process(self, img, **kwargs):
        self.gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, corners = cv2.findChessboardCorners(self.gray, (10, 6), None)
        corners2 = None
        if ret:
            criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
            corners2 = cv2.cornerSubPix(self.gray, corners, (11, 11), (-1, -1), criteria)
            self.objpoints.append(self.objp)
            self.imgpoints.append(corners2)
        return corners2

    def post_process(self, img, res, **kwargs):
        if res is not None:
            img = cv2.drawChessboardCorners(img, (10, 6), res, True)
        return img

    def reprojection(self, rvecs, tvecs, mtx, dist):
        mean_error = 0
        for i in range(len(self.objpoints)):
            imgpoints2, _ = cv2.projectPoints(self.objpoints[i], rvecs[i], tvecs[i], mtx, dist)
            error = cv2.norm(self.imgpoints[i], imgpoints2, cv2.NORM_L2) / len(imgpoints2)
            mean_error += error
        return mean_error / len(self.objpoints)

    def key_cb(self, key_code):
        """You can define this method to handle keyboard events by your processing module"""
        if key_code == ord('c'):
            print('Calibrating using {} images'.format(len(self.objpoints)))
            ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(self.objpoints, self.imgpoints, self.gray.shape[::-1],
                                                               None, None)
            print(dist.ravel())
            if ret:
                print("Reprojection error {}".format(self.reprojection(rvecs, tvecs, mtx, dist)))
        else:
            print('ignoring key, press c to calibrate or q to quit')